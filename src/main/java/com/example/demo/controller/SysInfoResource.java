package com.example.demo.controller;


import com.example.demo.entities.SysInfo;
import com.example.demo.services.SysInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@Service
public class SysInfoResource {
	//Logger log = LoggerFactory.getLogger(SysInfoResource.class);
	@Autowired
	private SysInfoService sysInfoService;

	   
	@RequestMapping("/sys")
    //@CrossOrigin
    public SysInfo getInfo() {
    	log.debug("getInfo start");
    	SysInfo sysInfo  = sysInfoService.getSysInfo();
    	log.debug("getInfo end");
		return sysInfo;
	}

}
