package com.example.demo.controller;

import com.example.demo.entities.User;
import com.example.demo.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@Service
// @Path("/users")
public class UserResource {
	//Logger log = LoggerFactory.getLogger(SysInfoResource.class);
	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	// @Produces("application/json")
	//@CrossOrigin

	public List<User> getUsers() {
		log.debug("getUsers start");
		final List<User> users = userService.findByPattern();

		log.debug("users size is {}",users.size());
		log.debug("getUsers end");
		return users;
	}

	//@PostMapping(value = "/users")
	//@CrossOrigin
	public void createUser(@RequestBody User user) {
		log.debug("createUser start");
		log.debug("createUser detail is {} ", user.toString());
		userService.save(user);
		log.debug("createUser end");
	}


	@RequestMapping(value = "/users/{id}",method = RequestMethod.DELETE)
	//@CrossOrigin
	public void deleteUser(@PathVariable("id") String userId) {
		//	System.out.println("Change.");
		log.debug("deleteUser start");
		log.debug("delete User ID is {}",userId);
		userService.delete(userId);
		log.debug("deleteUser end");

	}

//	@GET
//	@Path("/{id}")
//	@Produces("application/json")
//	@CrossOrigin
//	public Response getUsers(@PathParam("id") final String userId) {
//		User user = userService.findById(userId);
//		if (user == null) {
//			return Response.status(404).build();
//		}
//		return Response.status(200).entity(user).build();
//	}

//	@PUT
//	@Path("/{id}")
//	@Consumes("application/json")
//	@Produces("application/json")
//	@CrossOrigin
//	public Response updateUser(@PathParam("id") final String userId, final User user) {
//		user.setId(userId);
//		userService.update(user);
//		return Response.status(200).entity(user).build();
//	}

}
