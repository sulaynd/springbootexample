package com.example.demo.db;

import com.example.demo.entities.User;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;


@Service
public class SimpleDBImpl implements SimpleDB {
	Map<String, User> map = new ConcurrentHashMap<String, User>();

	public SimpleDBImpl() {

	}

	@Override
	public Map<String, User> getInstance() {
		return map;
	}
}
