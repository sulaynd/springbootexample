package com.example.demo.db;

import com.example.demo.entities.User;
import org.springframework.stereotype.Service;

import java.util.Map;
@Service
public interface SimpleDB {

	Map<String, User> getInstance();

}